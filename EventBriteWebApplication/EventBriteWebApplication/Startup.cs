﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EventBriteWebApplication.Startup))]
namespace EventBriteWebApplication
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
