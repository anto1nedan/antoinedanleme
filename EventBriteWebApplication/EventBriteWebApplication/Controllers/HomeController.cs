﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using EventBriteWebApplication.Models;
using System.Net;
using Newtonsoft.Json;


namespace EventBriteWebApplication.Controllers
{
    public class HomeController : Controller
    {

        private const string OAuthToken = "VBUSKKCQ2VTXKPOP34PX";
        private const string ApiUrl = @"https://www.eventbriteapi.com/v3/events/search/?token=";

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetEventList()
        {
            // Download the event list and parse
            // it then send this data to the model
            var eventList = LoadEventBriteAPIData(ApiUrl, OAuthToken);

            // Send the event list to the view
            return PartialView("EventList", eventList);
        }

        private List<EventModel> LoadEventBriteAPIData(string url, string token)
        {
            // Download the API JSON string
            var client = new WebClient();
            var json = client.DownloadString($"{url}{token}");

            // Parse the JSON data and create anonymous data types 
            dynamic model = JsonConvert.DeserializeObject(json);

            // Build the event list
            var events = new List<EventModel>();

            foreach (var eventModel in model.events)
            {
                // Using the anonymous data types
                // get the data and convert it
                // into the event model

                events.Add(new EventModel
                {
                    Name = Convert.ToString(eventModel.name.text),
                    Date = Convert.ToDateTime(eventModel.start.utc),
                    Url = Convert.ToString(eventModel.url),
                    Description = Convert.ToString(eventModel.description.html),
                    ThumbnailImageUrl = Convert.ToString(eventModel.logo.url)
                });
            }

            return events;
        }
    }
}