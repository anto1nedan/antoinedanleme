﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EventBriteWebApplication.Models
{
    public class EventModel
    {
        public string Name { get; set; }

        // Change the date format to (DD/MM/YYYY)
        [DisplayFormat(DataFormatString = "{0:d}")]
        public DateTime Date { get; set; }

        public string Url { get; set; }
        public string Description { get; set; }

        public string ThumbnailImageUrl { get; set; }
    }
}